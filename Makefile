# The produced executable filename
FILE	:= joytest

CROSS_COMPILE = human68k-

AS = $(CROSS_COMPILE)as
LD = $(CROSS_COMPILE)ld
CC = $(CROSS_COMPILE)gcc
OBJCOPY = $(CROSS_COMPILE)objcopy

AFLAGS		:= --register-prefix-optional
LDFLAGS		:= 
CFLAGS		:= -Wa,--register-prefix-optional -O2
LDSCRIPT	:= 
LIBS		:= -liocs -ldos
OCFLAGS		:= -O xfile

ifneq ($(LDSCRIPT),)
LDFLAGS		+=  -T $(LDSCRIPT)
endif

XFILE	:= $(FILE).x

OBJS	:= joytest.o aj.o
# asmfuncs.o 

%.o: %.s
	$(AS) $(AFLAGS) -o $@ $<
	
%.o: %.c
	$(CC) -c $(CFLAGS) $<

$(FILE): $(OBJS) $(CRT0)
	
	$(CC) $(OBJS) -o $(FILE) 
	$(OBJCOPY) $(OCFLAGS) $@ $(XFILE)

all: $(FILE)

clean:
	rm -f $(FILE) $(XFILE) $(OBJS)
