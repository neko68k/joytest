/*
Driver Application Sample by SHARP (1989) | File name: "AJ.C" ("AJ.X" after compilation)
Code:
*/
/*
	アナログジョイステイックを使うための簡単なプログラム

	1989/05/20	CC AjoySmp.c /W	でコンパイルの事

	ＳＴＡＲＴボタンを押すと終了します
*/



/*
＋０	ＤＣ．Ｗ	Ｓｔｉｃｋ　　　　ＵＰ／ＤＯＷＮ
＋２	ＤＣ．Ｗ	Ｓｔｉｃｋ　　　　ＬＥＦＴ／ＲＩＧＨＴ
＋４	ＤＣ．Ｗ	Ｔｈｒｏｔｔｌｅ　ＵＰ／ＤＯＷＮ
＋６	ＤＣ．Ｗ	Ｏｐｔｉｏｎ　　　ＵＰ／ＤＯＷＮ
＋８	ＤＣ．Ｗ	Ｔｒｉｇｇｅｒ　　（０でＯＮ）
			-|-|-|-|A|B|A'|B'|A+A'|B+B'|C|D|E1|E2|START|SELECT
			15						0
ｏｕｔ	Ｄ０．Ｌ＝　０．．．正常なデータ
		　－１．．．データ受信に失敗
*/

#include <stdbool.h>
#include "aj.h"

char cyberstick(short *work)
{
	if (AjoyGet(work)) {	/* error check */
		return(1);
	}
	return(0);
}

void AjoyInit(bool analog)
{
	if(!analog)
		asm("move	#0xF2,%%d0\n"
		"moveq	#1,%%d1\n"
		"moveq	#0,%%d2\n"
		"trap	#15\n"		
		: : :"d0", "d1", "d2");
	else
		asm("move	#0xF2,%%d0\n"
		"moveq	#1,%%d1\n"
		"moveq	#1,%%d2\n"
		"trap	#15\n"
		"move	#0xF2,%%d0\n"
		"moveq	#2,%%d1\n"
		"moveq	#0,%%d2\n"
		"trap	#15"
		: : :"d0", "d1", "d2");

/*	Original code
#asm
*-------------------------------*
*	アナログモードセット (Set analog mode)
*-------------------------------*
	moveq	#0xF2,d0		*ＩＯＣＳ番号
	moveq	#1,d1		*ＭＤ
	moveq	#1,d2		*アナログモード
	trap	#15		*ＩＯＣＳコール
*-------------------------------*
*	通信速度セット (Set comm speed)
*-------------------------------*
	moveq	#0xf2,d0		*ＩＯＣＳ番号
	moveq	#2,d1		*ＭＤ
	moveq	#0,d2		*最高速
	trap	#15		*ＩＯＣＳコール
#endasm
*/
}

AjoyGet(pointer)
short *pointer;
{
	asm("move	#0xF2,%%d0\n"
	"moveq	#0,%%d1\n"
	"move.l	%0,%%a1\n"
	"trap #15"
	: :"a" (pointer) :"d0", "d1", "a1");
	
/*
#asm
	moveq	#$f2,d0		*ＩＯＣＳ番号
	moveq	#0,d1		*ＭＤ
	move.l	8(A6),a1	*バッファアドレス
*8(A6) = pointer
	trap	#15		*ＩＯＣＳコール
*	tst.l	d0		*エラー？
#endasm
*/
}