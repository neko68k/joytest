// This is a simple program demonstrating how to interface with a CPSF or CPSF-MD joystick
// Using a second CPSF should follow the same technique but with joyport_b, IOC4 and PC5

// MegaDrive 3 button, 6 button based on
// https://www.cs.cmu.edu/~chuck/infopg/segasix.txt

// CPSF based on 
// https://gamesx.com/wiki/doku.php?id=controls:x686button

// MegaDrive multi-tap based on this document
// http://www.raphnet.net/divers/documentation/genesis_multitap.txt


#include <stdint.h>
#include <stdio.h>
#include <iocslib.h>
#include <doslib.h>
#include <stdbool.h>

#include "aj.h"

#define SEL_LO *joycontrolword = 0x8; *joycontrolword = 0xA;
#define SEL_HI *joycontrolword = 0x9; *joycontrolword = 0xB;

#define SELA_LO *joycontrolword = 0x8; 
#define SELB_LO *joycontrolword = 0xA;
#define SELA_HI *joycontrolword = 0x9;
#define SELB_HI *joycontrolword = 0xB;

#define MDMODE_MD3  0
#define MDMODE_MD6  1

#define MODE_CPSF 0 // cpsf, generics, etc
#define MODE_MD   1 // md auto
#define MODE_CA   2	// cyberstick analog
#define MODE_CD   3 // cyberstick digital

#define MODE_NC   4 // add 1 to this after adding a new MODE

volatile uint8_t *MFP_GPIP=	  (uint8_t *)0xE88001;
volatile uint8_t *MFP_AER=    (uint8_t *)0xE88003;
volatile uint8_t *MFP_IMRB=   (uint8_t *)0xE88015;
volatile uint8_t *MFP_IERB=	 (uint8_t *)0xE88009;

uint32_t *oldVsyncPtr = (uint32_t *)0;
uint32_t *vsyncVector = (uint32_t*)0x118;

volatile uint8_t *joyport_a = (uint8_t*)0xE9A001;
volatile uint8_t *joyport_b = (uint8_t*)0xE9A003;
volatile uint8_t *joyport_c = (uint8_t*)0xE9A005; // joy control
uint8_t *joycontrolword = (uint8_t*)0xE9A007; // if bit 7 = 0, bit manipulation. if bit 7 = 1, mode setting

uint8_t	joyport_c_old=0;

bool     cyberstick_inited = false;
int16_t  cyberstickData[5];

uint16_t buttons[4];
uint8_t  nibbles[4];
uint8_t  mdmode[2];
uint8_t  mode[2];
uint16_t read();


// this ought to support 3, 6 and the multitap
void genAuto(uint8_t which){
	/*    	    1      2      3      4     < cycle
			   H L    H L    H L    H L    < TH level
			 ---------------------------------------
	Nothing:   f f    f f    f f    f f
	3 Button:  f 3    f 3    f 3    f 3
	6 Button:  f 3    f 0    f f    f 3 
	Multi-tap: 3 f    3 f    3 f    3 f
	*/

	uint8_t byte = 0;
	volatile uint8_t *joyport = NULL;
	
	buttons[which] = 0;
	nibbles[0]=0;
	nibbles[1]=0;
	nibbles[2]=0;
	nibbles[3]=0;
	
	if(which == 0)
		joyport = joyport_a;
	else
		joyport = joyport_b;
	
	SEL_HI
	byte = (*joyport);
	buttons[which]= byte<<8;
	SEL_LO
	byte = (*joyport);
	nibbles[0] = byte&0xF;
	buttons[which]|= byte&0xF0;
	
	SEL_HI
	byte = (*joyport);
	SEL_LO
	byte = (*joyport);
	nibbles[1] = byte&0xF;
	
	SEL_HI
	byte = (*joyport);
	nibbles[2] = (byte&0xF)<<4;
	SEL_LO
	byte = (*joyport);
	nibbles[2] |= byte&0xF;
	
	if(nibbles[1] == 0x0)
		mdmode[which] = MDMODE_MD6;
	else if(nibbles[1] != 0xF && nibbles[1] != 0)
		mdmode[which] = MDMODE_MD3;
	else if(nibbles[0] == 0xF)
		mdmode[which] = MODE_NC;

	// 6 button
	if(mdmode[which] == MDMODE_MD6){
		SEL_HI
		buttons[which]|= nibbles[2]>>4;	// the low nibble here is XYZM

		byte = (*joyport);
		nibbles[3] |= byte&0xF;
	} 
	
	SEL_LO
	return;
}

// this is for reading a MegaDrive multi-tap
// this requires both controller ports for signaling
uint16_t readMulti(){
	// set both joystick ports as outputs
	
	// set TH(pin 7) to low
	
	// loop here 18 cycles(9 bytes)
	// toggle set TR(pin 9)
	// when TL(pin 6) goes low, data is ready
	// read 4 data pins (1-4)
	// loop until done
	
	// set TR high
	// set TH high
	
}

void cpsf(uint8_t which){
// unset PC4, this is the controller clock/button group select pin
		
	if(which == 0){
		SEL_HI
		// read joyport a bits
		buttons[which]= (*joyport_a);
		
		SEL_LO
		// read joyport a bits
		buttons[which]|= (*joyport_a)<<8;
	} else {
		SEL_HI
		// read joyport a bits
		buttons[which] = (*joyport_b);
		
		SEL_LO
		// read joyport a bits
		buttons[which] |= (*joyport_b)<<8;
		
	}

}

void printJoyData(uint8_t which)
{
	printf("Joystick %i:\n", which+1);
	if(mode[which]==MODE_MD){
		printf("Mode: MegaDrive          \n");
		genAuto(which);
	} else if(mode[which]==MODE_CPSF) {
		printf("Mode: CPSF               \n");
		cpsf(which);
	} else if(mode[which]==MODE_CA){
		printf("Mode: Cyberstick (analog)\n");
		
	} else if(mode[which]==MODE_CD){
		printf("Mode: Cyberstick (digital)\n");
		cpsf(which);
	}
	
	printf("Detected: ");
	if(mode[which] == MODE_MD && mdmode[which] == MDMODE_MD6){
		printf("MD 6 Button               \n");
	} else if(mode[which] == MODE_MD && mdmode[which] == MDMODE_MD3){
		printf("MD 3 Button               \n");
	} else if(mode[which] == MODE_NC){
		printf("N/C                       \n");
		buttons[which] = 0;
	} else {
		printf("---                      \n");
	}
	
	if(mode[which] == MODE_CA){
		// this probably doesn't work for 2P
		// need to check the docs
		if(!cyberstick(cyberstickData)){
			printf("\nStick Y: %04X\n", cyberstickData[0]);
			printf("Stick X: %04X\n", cyberstickData[1]);
			printf("Throttle: %04X\n", cyberstickData[2]);
			printf("Option: %04X\n", cyberstickData[3]);
			printf("Buttons: %04X\n\n", (uint16_t)~cyberstickData[4]);
		} 
	} else {
		//printf("\nNibbles: %2X, %2X, %2X, %2X\n", nibbles[0], nibbles[1], nibbles[2], nibbles[3]);
		printf("Buttons: %04X\n\n", (uint16_t)~buttons[which]);
	} 
}

int main(int argc, char *argv[])
{
	//uint8_t mode = 1;
	uint32_t usp = 0;
	uint16_t keyin = 0;
	uint8_t fnckeys[712];
	
	mode[0] = MODE_CPSF;
	mdmode[0] = MDMODE_MD3;
	mode[1] = MODE_CPSF;
	mdmode[1] = MDMODE_MD3;
	
	printf("\e[2J");
	// supervisor mode
	usp = _iocs_b_super(0);
	
	_dos_c_curoff();
	
	// set IOC4/5 to 1, disables joystick 1/2 processing
	// preserves ADPCM settings
	joyport_c_old = *joyport_c;
	*joyport_c = 48|joyport_c_old; // enable select pins (PC4,PC5) as outputs
	
	// the emulator doesn't care but hardware
	// wants the SEL line to be low to begin with.
	SEL_LO
	
	
	while(1)
	{
		// wait for vblank
		while (*MFP_GPIP & 0x10);
		
		// move cursor to output location
		printf("\e[0;0H");
		
		// clear screen
	
		printf("Controller Tester V3\n");
		printf("neko68k 2018\n");
		printf("--------------------------------------------------------------------------\n\n");
		
		printf("Keys:\n");
		printf("C = CPSF / MSX / MSX+Start / Magical Pad / XPD-1LR\n");
		printf("M = MegaDrive Auto Detect\n");
		printf("D = Cyberstick (digital)\n");
		printf("A = Cyberstick (analog)\n");
		printf("Shift+above keys for Joystick 2 mode setting\n");
		printf("Press ESC to quit.\n\n");
		
		printf("--------------------------------------------------------------------------\n\n");
	
		printJoyData(0);
		printJoyData(1);
		
		printf("--------------------------------------------------------------------------\n");

		// handle keyboard
		keyin = _dos_inpout(0xFF);
		if( keyin == 0x1B){
				SEL_LO
				*joyport_c = joyport_c_old;
				_dos_c_curon();
				_iocs_b_super(usp);
				printf("\e[2J");
				return 0;
		} else if (keyin==0x63){
				// switch mode to Generic/CPSF
				printf("\e[2J");
				mode[0] = MODE_CPSF;
		} else if (keyin==0x6D){
				// switch mode to MD Auto Detect
				printf("\e[2J");
				mode[0] = MODE_MD;
		} else if (keyin==0x61){
				// switch mode to Cyberstick (analog)
				printf("\e[2J");
				mode[0] = MODE_CA;
				AjoyInit(true);
		} else if (keyin==0x64){
				// switch mode to Cyberstick (digital)
				printf("\e[2J");
				mode[0] = MODE_CD;	
				AjoyInit(false);
// shift+key
		} else if (keyin==0x43){
				// switch mode to Generic/CPSF
				printf("\e[2J");
				mode[1] = MODE_CPSF;
		} else if (keyin==0x4D){
				// switch mode to MD Auto Detect
				printf("\e[2J");
				mode[1] = MODE_MD;
		} else if (keyin==0x41){
				// switch mode to Cyberstick (analog)
				printf("\e[2J");
				mode[1] = MODE_CA;
				AjoyInit(true);
		} else if (keyin==0x44){
				// switch mode to Cyberstick (digital)
				printf("\e[2J");
				mode[1] = MODE_CD;	
				AjoyInit(false);
		} 

	}
}



